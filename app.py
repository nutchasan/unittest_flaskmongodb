from flask import Flask, render_template, request
# import mongoengine as me
from models.users import User 
from forms.user import UserForm 
from mongoengine.queryset.visitor import Q


app = Flask(__name__)
app.config['SECRET_KEY'] = '123eiei123'

@app.route('/')
def hello_world():
    # user = User.objects().get(firstname="Jakee")
    # return 'Hello, World!'
    return 'Hello World'



@app.route('/user',  methods=["GET", "POST"])
def user():
    user = User.objects().get(firstname="Jakee")
    return render_template('user.html', user=user)

# @app.route('/add_user', methods=["GET", "POST"])
# def add_user():
#     form = UserForm
#     new_user = User()

#     if not form.validate_on_submit():
#         return render_template('add_user.html',
#                                form=form)



@app.route('/add_user',  methods=["GET", "POST"])
def add_user():
    # user = User.objects().get(firstname="Jakee")
    form = UserForm()
    if not form.validate_on_submit():
        return render_template('add_user.html', 
                               user=user,
                               form=form)

    new_user = User()
    new_user.firstname = form.firstname.data
    new_user.lastname = form.lastname.data
    new_user.age = form.age.data
    new_user.status = True

    new_user.save()

    return 'Success'

@app.route('/search', methods=["GET"])
def search():
    form = UserForm()
    search_firstname = request.args.get('search_firstname')
    search_age = request.args.get('search_age')
    result = User.objects(Q(firstname__icontains=search_firstname) | 
                            Q(age__gte=search_age))
    print(result)
    return render_template('add_user.html',
                             users=result,
                             form=form)

if __name__ == '__main__':
    app.run(debug=True)