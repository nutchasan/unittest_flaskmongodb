from flask_wtf import FlaskForm
from wtforms import fields

class UserForm(FlaskForm):
    firstname = fields.TextField()
    lastname = fields.TextField()
    age = fields.IntegerField()