import mongoengine as me


class Address(me.EmbeddedDocument):
    home_number = me.StringField()
    city = me.StringField()
    postcode = me.StringField()


class User(me.Document):
    meta = {'collection': 'users'}

    firstname = me.StringField()
    lastname = me.StringField()
    age = me.IntField()
    status = me.BooleanField()
    address = me.EmbeddedDocumentField('Address', default=Address())

me.connect('unittest_db')



# new_user = User()
# new_user.firstname = 'Donut'
# new_user.lastname = 'Jantarah'
# new_user.age = 21
# new_user.status = True

# user_address = Address()
# user_address.home_number = '26'
# user_address.city = 'Hatyai'
# user_address.postcode = '90250'

# new_user.address = user_address

# new_user.save()

# user = User.objects().get(id='5da7f40f4f827b021fca1f6b')
# user.delete()


print('Success')



























# class Address(me.EmbeddedDocument):
#     home_number = me.StringField()
#     city = me.StringField()
#     postcode = me.StringField()
#     tel = me.StringField()

# class User(me.Document):
#     meta = {'collection': 'users'}

#     firstname = me.StringField()
#     lastname = me.StringField()
#     age = me.IntField()
#     status = me.BooleanField()
#     address = me.EmbeddedDocumentField("Address", default=Address())
    
# me.connect("user_unittest")

# user_address = Address()
# user_address.home_number = "26"
# user_address.city = "Hadyai"
# user_address.postcode = "90250"
# user_address.tel = "074555666"

# user = User()
# user.firstname = "Jakee"
# user.lastname = "Eiei"
# user.age = 21
# user.status = True
# user.address =  user_address



# user.save()


# users = User.objects()
# users.update(status=True)




